package com.sber.java15.spring.springprojectlibrary.library.service;

import com.sber.java15.spring.springprojectlibrary.library.constants.Errors;
import com.sber.java15.spring.springprojectlibrary.library.dto.AddBookDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.AuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectlibrary.library.mapper.AuthorMapper;
import com.sber.java15.spring.springprojectlibrary.library.mapper.GenericMapper;
import com.sber.java15.spring.springprojectlibrary.library.model.Author;
import com.sber.java15.spring.springprojectlibrary.library.model.Book;
import com.sber.java15.spring.springprojectlibrary.library.repository.AuthorRepository;
import com.sber.java15.spring.springprojectlibrary.library.repository.BookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Set;

@Service
public class AuthorService
      extends GenericService<Author, AuthorDTO> {
    private final AuthorRepository authorRepository;
    
    public AuthorService(AuthorRepository authorRepository,
                         AuthorMapper authorMapper) {
        super(authorRepository, authorMapper);
        this.authorRepository = authorRepository;
    }
    
    public AuthorDTO addBook(final AddBookDTO addBookDTO) {
        AuthorDTO author = getOne(addBookDTO.getAuthorId());
        author.getBookIDs().add(addBookDTO.getBookId());
        update(author);
        return author;
    }
    
    public Page<AuthorDTO> searchAuthors(final String fio,
                                         Pageable pageable) {
        Page<Author> authors = authorRepository.findAllByAuthorFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<AuthorDTO> result = mapper.toDTOs(authors.getContent());
        return new PageImpl<>(result, pageable, authors.getTotalElements());
    }
    
    @Override
    public void deleteSoft(Long objectId) throws MyDeleteException {
        Author author = authorRepository.findById(objectId).orElseThrow(
              () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        boolean authorCanBeDeleted = authorRepository.checkAuthorForDeletion(objectId);
        if (authorCanBeDeleted) {
            markAsDeleted(author);
            List<Book> books = author.getBooks();
            if (books != null && books.size() > 0) {
                books.forEach(this::markAsDeleted);
            }
            authorRepository.save(author);
        }
        else {
            throw new MyDeleteException(Errors.Authors.AUTHOR_DELETE_ERROR);
        }
    }
    
    public void restore(Long objectId) {
        Author author = authorRepository.findById(objectId).orElseThrow(
              () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(author);
        List<Book> books = author.getBooks();
        if (books != null && books.size() > 0) {
            books.forEach(this::unMarkAsDeleted);
        }
        authorRepository.save(author);
    }
}
