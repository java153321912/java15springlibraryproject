//package com.sber.java15.spring.springprojectlibrary.dbexample.dao;
//
//import com.sber.java15.spring.springprojectlibrary.dbexample.DB.DBConnection;
//import com.sber.java15.spring.springprojectlibrary.dbexample.model.Book;
//
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//public class BookDaoJDBC {
//
//    //select * from books where book_id = :bookId
//    public Book findBookById(Integer bookId) {
//        try (Connection connection = DBConnection.INSTANCE.newConnection()) {
//            if (connection != null) {
//                System.out.println("Ура! Мы подключились к БД!!!!");
//            }
//            else {
//                System.out.println("БД недоступна");
//            }
//            String select = "select * from books where id = ?";
//            PreparedStatement selectQuery = connection.prepareStatement(select);
//            selectQuery.setInt(1, bookId);
//            ResultSet resultSet = selectQuery.executeQuery();
////            List<Book> books = new ArrayList<>();
//            while (resultSet.next()) {
//                Book book = new Book();
//                book.setAuthor(resultSet.getString("author"));
//                book.setTitle(resultSet.getString("title"));
//                book.setDateAdded(resultSet.getDate("date_added"));
////                books.add(book);
//                System.out.println(book);
//                return book;
//            }
////            return books.size() > 0 ? books : Collections.emptyList();
//
//        }
//        catch (SQLException e) {
//            System.out.println("Error: " + e.getMessage());
//        }
//        return null;
//    }
//
////    public Book findBookByTitle(String title) {
////        title = "//execute 'drop database'";
////    }
//}
