package com.sber.java15.spring.springprojectlibrary.library.REST.controller;

import com.sber.java15.spring.springprojectlibrary.library.dto.AddBookDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookDTO;
import com.sber.java15.spring.springprojectlibrary.library.model.Book;
import com.sber.java15.spring.springprojectlibrary.library.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
@Tag(name = "Книги",
     description = "Контроллер для работы с книгами библиотеки")
public class BookController
      extends GenericController<Book, BookDTO> {
    public BookController(BookService bookService) {
        super(bookService);
    }
    
    @Operation(description = "Добавить книгу к автору")
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDTO> addAuthor(@RequestParam(value = "bookId") Long bookId,
                                             @RequestParam(value = "authorId") Long authorId) {
        AddBookDTO bookDTO = new AddBookDTO();
        bookDTO.setAuthorId(authorId);
        bookDTO.setBookId(bookId);
        return ResponseEntity.status(HttpStatus.OK).body(((BookService) service).addAuthor(bookDTO));
    }
}
