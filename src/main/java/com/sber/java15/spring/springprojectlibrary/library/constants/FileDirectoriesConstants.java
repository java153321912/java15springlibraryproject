package com.sber.java15.spring.springprojectlibrary.library.constants;

public interface FileDirectoriesConstants {
    String BOOKS_UPLOAD_DIRECTORY = "files/books";
}
