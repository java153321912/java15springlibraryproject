package com.sber.java15.spring.springprojectlibrary.library.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String LIBRARIAN = "LIBRARIAN";
}
