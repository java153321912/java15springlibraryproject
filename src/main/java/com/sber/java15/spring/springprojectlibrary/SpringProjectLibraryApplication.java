package com.sber.java15.spring.springprojectlibrary;

//import com.sber.java15.spring.springprojectlibrary.dbexample.DB.DBConnection;
//import com.sber.java15.spring.springprojectlibrary.dbexample.dao.BookDAOBean;
//import com.sber.java15.spring.springprojectlibrary.dbexample.dao.BookDaoJDBC;
//import com.sber.java15.spring.springprojectlibrary.dbexample.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

//JDBC - Java DataBase Connection
@SpringBootApplication
//@EnableScheduling
public class SpringProjectLibraryApplication
      implements CommandLineRunner {
//    private BookDAOBean bookDAOBean;

//Инжект бина через конструктор
//    public SpringProjectLibraryApplication(BookDAOBean bookDAOBean) {
//        this.bookDAOBean = bookDAOBean;
//    }
    
    //Инжект бина через сеттер
//    @Autowired
//    public void setBookDAOBean(BookDAOBean bookDAOBean) {
//        this.bookDAOBean = bookDAOBean;
//    }
    
    //JDBC обертка от спринга (зависимость spring-boot-starter-jdbc)
//    @Autowired
//    private NamedParameterJdbcTemplate jdbcTemplate;
    
    public static void main(String[] args) {
        SpringApplication.run(SpringProjectLibraryApplication.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Swagger runs at http://localhost:8080/swagger-ui/index.html");
// 1 шаг
//        BookDaoJDBC bookDaoJDBC = new BookDaoJDBC();
//        bookDaoJDBC.findBookById(1);

// 2 шаг
//        BookDAOBean bookDAOBean = new BookDAOBean(DBConnection.INSTANCE.newConnection());
//        bookDAOBean.findBookById(1);
// 3 шаг
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDBConfigContext.class);
//        BookDAOBean bookDAOBean = ctx.getBean(BookDAOBean.class);
//        bookDAOBean.findBookById(1);
//        bookDAOBean.findBookById(1);

//        List<Book> bookList = jdbcTemplate.query("select * from books",
//                                                 (rs, rowNum) -> new Book(
//                                                       rs.getInt("id"),
//                                                       rs.getString("title"),
//                                                       rs.getString("author"),
//                                                       rs.getDate("date_added")
//                                                 ));
//        bookList.forEach(System.out::println);
    }
}
