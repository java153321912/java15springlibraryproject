package com.sber.java15.spring.springprojectlibrary.library.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddBookDTO {
    Long bookId;
    Long authorId;
}
