package com.sber.java15.spring.springprojectlibrary.library.mapper;

import com.sber.java15.spring.springprojectlibrary.library.dto.BookWithAuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.model.Book;
import com.sber.java15.spring.springprojectlibrary.library.model.GenericModel;
import com.sber.java15.spring.springprojectlibrary.library.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class BookWithAuthorsMapper
      extends GenericMapper<Book, BookWithAuthorDTO> {
    
    private AuthorRepository authorRepository;
    
    protected BookWithAuthorsMapper(ModelMapper modelMapper,
                                    AuthorRepository authorRepository) {
        super(Book.class, BookWithAuthorDTO.class, modelMapper);
        this.authorRepository = authorRepository;
    }
    
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Book.class, BookWithAuthorDTO.class)
              .addMappings(m -> m.skip(BookWithAuthorDTO::setAuthorIds)).setPostConverter(toDTOConverter());
        
        modelMapper.createTypeMap(BookWithAuthorDTO.class, Book.class)
              .addMappings(m -> m.skip(Book::setAuthors)).setPostConverter(toEntityConverter());
    }
    
    @Override
    protected void mapSpecificFields(BookWithAuthorDTO source, Book destination) {
        destination.setAuthors(authorRepository.findAllById(source.getAuthorIds()));
    }
    
    @Override
    protected void mapSpecificFields(Book source, BookWithAuthorDTO destination) {
        destination.setAuthorIds(getIds(source));
    }
    
    @Override
    protected List<Long> getIds(Book entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
               ? null
               : entity.getAuthors().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toList());
    }
}
