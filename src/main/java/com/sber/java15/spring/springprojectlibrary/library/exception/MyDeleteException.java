package com.sber.java15.spring.springprojectlibrary.library.exception;

public class MyDeleteException
      extends Exception {
    public MyDeleteException(final String message) {
        super(message);
    }
}
