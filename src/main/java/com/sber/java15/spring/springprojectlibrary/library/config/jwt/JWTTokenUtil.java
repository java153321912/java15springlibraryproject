package com.sber.java15.spring.springprojectlibrary.library.config.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

/*
    l123lsd7ti716t2oe8.glhudgqs786ret9i7o1y2f3.ivykdkfay6rdfi
    Header = l123lsd7ti716t2oe8 => {"alg":"HS256", "typ":"JWT"}
    Payload (JWT-Claims - заявки) = glhudgqs786ret9i7o1y2f3 => {"user_id":1, "login":"andy_user", "role":"ROLE_USER", ....}
    Signature = ivykdkfay6rdfi
     */

@Component
@Slf4j
public class JWTTokenUtil {
    //7 * 24 * 60 * 1000 = 1 неделя в миллисекундах (время жизни токена)
    public static final long JWT_TOKEN_VALIDITY = 604800000;
    //секрет для формирования подписи токена
    private final String secret = "zdtlD3JK56m6wTTgsNFhqzjqP";
    
    private static final ObjectMapper objectMapper = getDefaultObjectMapper();
    
    private static ObjectMapper getDefaultObjectMapper() {
        return new ObjectMapper();
    }
    
    //Создаем токен и кладем в payload(claims) информацию о ПОЛЬЗОВАТЕЛЕ, которую взяли из CustomUserDetails.toString()
    //настраиваем время жизни, алгоритм шифрования, когда выдан
    /*
    payload = { "user_id": "1",
                "username": "andy_gavrilov",
                "user_role": "USER",
                "password": "asdaey1u2g3k2hedvbqwkegrv1i2uyv"
                }
     */
    public String generateToken(final UserDetails payload) {
        
        return Jwts.builder()
              .setSubject(payload.toString())
              .setIssuedAt(new Date(System.currentTimeMillis()))
              .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
              .signWith(SignatureAlgorithm.HS512, secret)
              .compact();
    }
    
    //проверка на то, что токен истек, или нет
    private Boolean isTokenExpired(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }
    
    //нужно достать expirationDate из токена
    private Date getExpirationDateFromToken(final String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }
    
    //Подтверждение токена
    public Boolean validateToken(final String token,
                                 UserDetails userDetails) {
        final String userName = getUsernameFromToken(token);
        return (userName.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    
    //Достаем username из токена (из claims)
    public String getUsernameFromToken(final String token) {
        String claim = getClaimsFromToken(token, Claims::getSubject);
        JsonNode claimJSON = null;
        try {
            claimJSON = objectMapper.readTree(claim);
        }
        catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUserNameFromToken(): {}", e.getMessage());
        }
        
        if (claimJSON != null) {
            return claimJSON.get("username").asText();
        }
        else {return null;}
    }
    
    //Достаем role из токена (из claims)
    public String getRoleFromToken(final String token) {
        String claim = getClaimsFromToken(token, Claims::getSubject);
        JsonNode claimJSON = null;
        try {
            claimJSON = objectMapper.readTree(claim);
        }
        catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUserNameFromToken(): {}", e.getMessage());
        }
        
        if (claimJSON != null) {
            return claimJSON.get("user_role").asText();
        }
        else {return null;}
    }
    
    private <T> T getClaimsFromToken(final String token, Function<Claims, T> claimResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimResolver.apply(claims);
    }
    
    //для получения любой информации из токена, нужно предъявить секретный ключ
    private Claims getAllClaimsFromToken(final String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }
    
    
}
