package com.sber.java15.spring.springprojectlibrary.library.repository;

import com.sber.java15.spring.springprojectlibrary.library.model.BookRentInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRentInfoRepository
      extends GenericRepository<BookRentInfo> {
    Page<BookRentInfo> getBookRentInfoByUserId(Long id,
                                               Pageable pageRequest);
}
