package com.sber.java15.spring.springprojectlibrary.library.dto;

import com.sber.java15.spring.springprojectlibrary.library.model.Author;
import com.sber.java15.spring.springprojectlibrary.library.model.Book;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthorDTO
        extends GenericDTO {
    private String authorFio;
    private LocalDate birthDate;
    private String description;
    private List<Long> bookIDs;


    public AuthorDTO(Author author) {
        this.birthDate = author.getBirthDate();
        this.createdBy = author.getCreatedBy();
        this.authorFio = author.getAuthorFio();
        this.description = author.getDescription();
        this.createdWhen = author.getCreatedWhen();
        this.id = author.getId();
        List<Book> books = author.getBooks();
        List<Long> bookIds = new ArrayList<>();
        books.forEach(b -> bookIds.add(b.getId()));
        this.bookIDs = bookIds;
        this.isDeleted = false;
    }
//
//    public static List<AuthorDTO> getAuthorDTOs(List<Author> authors) {
//        List<AuthorDTO> authorDTOS = new ArrayList<>();
//        for (Author author : authors) {
//            authorDTOS.add(new AuthorDTO(author));
//        }
//        return authorDTOS;
//    }

}
