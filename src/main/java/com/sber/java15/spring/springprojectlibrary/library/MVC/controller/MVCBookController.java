package com.sber.java15.spring.springprojectlibrary.library.MVC.controller;

import com.sber.java15.spring.springprojectlibrary.library.dto.AuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookSearchDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookWithAuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectlibrary.library.service.BookService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.sber.java15.spring.springprojectlibrary.library.constants.UserRolesConstants.ADMIN;

@Slf4j
@Controller
@RequestMapping("/books")
public class MVCBookController {
    private final BookService bookService;
    
    public MVCBookController(BookService bookService) {
        this.bookService = bookService;
    }
    
    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "bookTitle"));
        Page<BookWithAuthorDTO> books;
        final String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            books = bookService.getAllBooksWithAuthors(pageRequest);
        }
        else {
            books = bookService.getAllNotDeletedBooksWithAuthors(pageRequest);
        }
        model.addAttribute("books", books);
        model.addAttribute("exception", exception);
        return "books/viewAllBooks";
    }
    
    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("book", bookService.getBookWithAuthors(id));
        return "books/viewBook";
    }
    
    @GetMapping("/add")
    public String create() {
        return "books/addBook";
    }
    
    @PostMapping("/add")
    public String create(@ModelAttribute("bookForm") BookDTO newBook,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            bookService.create(newBook, file);
        }
        else {
            bookService.create(newBook);
        }
        return "redirect:/books";
    }
    
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("book", bookService.getOne(id));
        return "books/updateBook";
    }
    
    @PostMapping("/update")
    public String update(@ModelAttribute("bookForm") BookDTO bookDTO,
                         @RequestParam MultipartFile file) {
        log.info(bookDTO.toString());
        if (file != null && file.getSize() > 0) {
            bookService.update(bookDTO, file);
        }
        else {
            bookService.update(bookDTO);
        }
        return "redirect:/books";
    }
    
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        bookService.deleteSoft(id);
        return "redirect:/books";
    }
    
    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        bookService.restore(id);
        return "redirect:/books";
    }
    
    @PostMapping("/search")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("bookSearchForm") BookSearchDTO bookSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("books", bookService.findBooks(bookSearchDTO, pageRequest));
        return "books/viewAllBooks";
    }
    
    /**
     * Метод для поиска книги по ФИО автора (редирект по кнопке "Посмотреть книги" на странице автора)
     *
     * @param page      - текущая страница
     * @param pageSize  - количество объектов на странице
     * @param authorDTO - ДТО автора
     * @param model     - модель
     * @return - форму со списком всех книг подходящих под критерии (по фио автора)
     */
    @PostMapping("/search/author")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("authorSearchForm") AuthorDTO authorDTO,
                              Model model) {
        BookSearchDTO bookSearchDTO = new BookSearchDTO();
        bookSearchDTO.setAuthorFio(authorDTO.getAuthorFio());
        return searchBooks(page, pageSize, bookSearchDTO, model);
    }
    
    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadBook(@Param(value = "bookId") Long bookId) throws IOException {
        BookDTO bookDTO = bookService.getOne(bookId);
        Path path = Paths.get(bookDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        
        return ResponseEntity.ok()
              .headers(headers(path.getFileName().toString()))
              .contentLength(path.toFile().length())
              .contentType(MediaType.APPLICATION_OCTET_STREAM)
//              .contentType(MediaType.parseMediaType("application/octet-stream"))
              .body(resource);
    }
    
    private HttpHeaders headers(final String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store");
        headers.add("Expires", "0");
        return headers;
    }
    
    @ExceptionHandler({MyDeleteException.class, AccessDeniedException.class, NotFoundException.class})
    public RedirectView handleError(HttpServletRequest request,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос " + request.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/books", true);
    }
}
