package com.sber.java15.spring.springprojectlibrary.library.mapper;

import com.sber.java15.spring.springprojectlibrary.library.dto.AuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.model.Author;
import com.sber.java15.spring.springprojectlibrary.library.model.GenericModel;
import com.sber.java15.spring.springprojectlibrary.library.repository.BookRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AuthorMapper
      extends GenericMapper<Author, AuthorDTO> {
    
    private final BookRepository bookRepository;
    
    protected AuthorMapper(ModelMapper modelMapper,
                           BookRepository bookRepository) {
        super(Author.class, AuthorDTO.class, modelMapper);
        this.bookRepository = bookRepository;
    }
    
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Author.class, AuthorDTO.class)
              .addMappings(m -> m.skip(AuthorDTO::setBookIDs)).setPostConverter(toDTOConverter());
        
        modelMapper.createTypeMap(AuthorDTO.class, Author.class)
              .addMappings(m -> m.skip(Author::setBooks)).setPostConverter(toEntityConverter());
    }
    
    @Override
    protected void mapSpecificFields(AuthorDTO source, Author destination) {
        if (!Objects.isNull(source.getBookIDs())) {
            destination.setBooks(bookRepository.findAllById(source.getBookIDs()));
        }
        else {
            destination.setBooks(Collections.emptyList());
        }
    }
    
    @Override
    protected void mapSpecificFields(Author source, AuthorDTO destination) {
        destination.setBookIDs(getIds(source));
    }
    
    @Override
    protected List<Long> getIds(Author source) {
        return Objects.isNull(source) || Objects.isNull(source.getBooks())
               ? Collections.emptyList()
               : source.getBooks().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toList());
    }
}
