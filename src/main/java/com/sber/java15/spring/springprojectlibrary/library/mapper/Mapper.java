package com.sber.java15.spring.springprojectlibrary.library.mapper;

import com.sber.java15.spring.springprojectlibrary.library.dto.GenericDTO;
import com.sber.java15.spring.springprojectlibrary.library.model.GenericModel;

import java.util.List;

/*
Интерфейс, имеющий основной набор методов для преобразования из сущности в DTO
 */
public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    
    E toEntity(D dto);
    
    D toDTO(E entity);
    
    List<E> toEntities(List<D> dtos);
    
    List<D> toDTOs(List<E> entities);
    
}
