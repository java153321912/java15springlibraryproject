package com.sber.java15.spring.springprojectlibrary.library.dto;

import com.sber.java15.spring.springprojectlibrary.library.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookSearchDTO {
    private String bookTitle;
    private String authorFio;
    private Genre genre;
}
