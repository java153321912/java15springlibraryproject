package com.sber.java15.spring.springprojectlibrary.library.service;

import com.sber.java15.spring.springprojectlibrary.library.constants.Errors;
import com.sber.java15.spring.springprojectlibrary.library.dto.AddBookDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookSearchDTO;
import com.sber.java15.spring.springprojectlibrary.library.dto.BookWithAuthorDTO;
import com.sber.java15.spring.springprojectlibrary.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectlibrary.library.mapper.BookMapper;
import com.sber.java15.spring.springprojectlibrary.library.mapper.BookWithAuthorsMapper;
import com.sber.java15.spring.springprojectlibrary.library.model.Author;
import com.sber.java15.spring.springprojectlibrary.library.model.Book;
import com.sber.java15.spring.springprojectlibrary.library.model.GenericModel;
import com.sber.java15.spring.springprojectlibrary.library.repository.AuthorRepository;
import com.sber.java15.spring.springprojectlibrary.library.repository.BookRepository;
import com.sber.java15.spring.springprojectlibrary.library.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookService
      extends GenericService<Book, BookDTO> {
    private final BookRepository repository;
    
    private final BookWithAuthorsMapper bookWithAuthorsMapper;
    
    protected BookService(BookRepository repository,
                          BookMapper mapper,
                          BookWithAuthorsMapper bookWithAuthorsMapper) {
        super(repository, mapper);
        this.repository = repository;
        this.bookWithAuthorsMapper = bookWithAuthorsMapper;
    }
    
    public Page<BookWithAuthorDTO> getAllBooksWithAuthors(Pageable pageable) {
        Page<Book> booksPaginated = repository.findAll(pageable);
        List<BookWithAuthorDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }
    
    public Page<BookWithAuthorDTO> getAllNotDeletedBooksWithAuthors(Pageable pageable) {
        Page<Book> booksPaginated = repository.findAllByIsDeletedFalse(pageable);
        List<BookWithAuthorDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageable, booksPaginated.getTotalElements());
    }
    
    public BookDTO addAuthor(final AddBookDTO addBookDTO) {
        BookDTO book = getOne(addBookDTO.getBookId());
        book.getAuthorIds().add(addBookDTO.getAuthorId());
        update(book);
        return book;
    }
    
    public Page<BookWithAuthorDTO> findBooks(BookSearchDTO bookSearchDTO,
                                             Pageable pageRequest) {
        String genre = bookSearchDTO.getGenre() != null ? String.valueOf(bookSearchDTO.getGenre().ordinal()) : null;
        Page<Book> booksPaginated = repository.searchBooks(genre,
                                                           bookSearchDTO.getBookTitle(),
                                                           bookSearchDTO.getAuthorFio(),
                                                           pageRequest);
        List<BookWithAuthorDTO> result = bookWithAuthorsMapper.toDTOs(booksPaginated.getContent());
        return new PageImpl<>(result, pageRequest, booksPaginated.getTotalElements());
    }
    
    /**
     * @param id
     * @return
     */
    public BookWithAuthorDTO getBookWithAuthors(final Long id) {
        return bookWithAuthorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }
    
    public BookDTO create(final BookDTO newBook,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        newBook.setOnlineCopyPath(fileName);
        newBook.setCreatedWhen(LocalDateTime.now());
        newBook.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(newBook)));
    }
    
    public BookDTO update(final BookDTO updatedBook,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        updatedBook.setOnlineCopyPath(fileName);
        //TODO: переделать обновление книги (стирание авторов)
        Book book = repository.findById(updatedBook.getId()).get();
        List<Long> ids = new ArrayList<>();
        for (Author author : book.getAuthors()){
            ids.add(author.getId());
        }
        updatedBook.setAuthorIds(ids);
        return mapper.toDTO(repository.save(mapper.toEntity(updatedBook)));
    }
    
    @Override
    public void deleteSoft(final Long id) throws MyDeleteException {
        Book book = repository.findById(id).orElseThrow(() -> new NotFoundException("Книги не найдено"));
        boolean bookCanBeDeleted = repository.checkBookForDeletion(id);
//        boolean b1 = repository.findBookByIdAndBookRentInfosReturnedFalseAndIsDeletedFalse(id) == null;
        if (bookCanBeDeleted) {
            markAsDeleted(book);
            repository.save(book);
        }
        else {
            throw new MyDeleteException(Errors.Book.BOOK_DELETE_ERROR);
        }
    }
}
