package com.sber.java15.spring.springprojectlibrary.library.MVC.controller;

import com.sber.java15.spring.springprojectlibrary.library.dto.BookRentInfoDTO;
import com.sber.java15.spring.springprojectlibrary.library.service.BookRentInfoService;
import com.sber.java15.spring.springprojectlibrary.library.service.BookService;
import com.sber.java15.spring.springprojectlibrary.library.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@Hidden
@RequestMapping("/rent")
public class MVCBookRentInfoController {
    
    private final BookRentInfoService bookRentInfoService;
    private final BookService bookService;
    
    public MVCBookRentInfoController(BookRentInfoService bookRentInfoService,
                                     BookService bookService) {
        this.bookRentInfoService = bookRentInfoService;
        this.bookService = bookService;
    }
    
    @GetMapping("/book/{bookId}")
    public String rentBook(@PathVariable Long bookId,
                           Model model) {
        model.addAttribute("book", bookService.getOne(bookId));
        return "userBooks/rentBook";
    }
    
    @PostMapping("/book")
    public String rentBook(@ModelAttribute("rentBookInfo") BookRentInfoDTO rentBookInfoDTO) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        rentBookInfoDTO.setUserId(Long.valueOf(customUserDetails.getUserId()));
        bookRentInfoService.rentBook(rentBookInfoDTO);
        return "redirect:/rent/user-books/" + customUserDetails.getUserId();
    }
    
    @GetMapping("/return-book/{id}")
    public String returnBook(@PathVariable Long id) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        bookRentInfoService.returnBook(id);
        return "redirect:/rent/user-books/" + customUserDetails.getUserId();
    }
    
    @GetMapping("/user-books/{id}")
    public String userBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "5") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<BookRentInfoDTO> rentInfoDTOPage = bookRentInfoService.listUserRentBooks(id, pageRequest);
        model.addAttribute("rentBooks", rentInfoDTOPage);
        model.addAttribute("userId", id);
        return "userBooks/viewAllUserBooks";
    }
}
