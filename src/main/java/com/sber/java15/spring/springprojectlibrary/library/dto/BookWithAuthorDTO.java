package com.sber.java15.spring.springprojectlibrary.library.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BookWithAuthorDTO
      extends BookDTO {
    private Set<AuthorDTO> authors;
}
