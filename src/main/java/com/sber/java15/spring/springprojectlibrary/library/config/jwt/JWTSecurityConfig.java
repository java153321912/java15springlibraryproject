//package com.sber.java15.spring.springprojectlibrary.library.config.jwt;
//
//import com.sber.java15.spring.springprojectlibrary.library.service.userdetails.CustomUserDetailsService;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import static com.sber.java15.spring.springprojectlibrary.library.constants.SecurityConstants.RESOURCES_WHITE_LIST;
//import static com.sber.java15.spring.springprojectlibrary.library.constants.SecurityConstants.USERS_REST_WHITE_LIST;
//import static com.sber.java15.spring.springprojectlibrary.library.constants.UserRolesConstants.*;
//
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class JWTSecurityConfig {
//    private final JWTTokenFilter jwtTokenFilter;
//    private final CustomUserDetailsService customUserDetailsService;
//
//    public JWTSecurityConfig(JWTTokenFilter jwtTokenFilter,
//                             CustomUserDetailsService customUserDetailsService) {
//        this.jwtTokenFilter = jwtTokenFilter;
//        this.customUserDetailsService = customUserDetailsService;
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//        return http.cors(AbstractHttpConfigurer::disable)
//              .csrf(AbstractHttpConfigurer::disable)
//              //Настройка http-запросов - кому/куда можно/нельзя
//              .authorizeHttpRequests((requests) -> requests
//                                           .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(USERS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers("/rest/authors/**").hasAnyRole(ADMIN, USER)
//                                           .anyRequest().authenticated()
//                                    )
//              .exceptionHandling()
//              // .authenticationEntryPoint()
//              .and()
//              .sessionManagement(
//                    session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                                )
//              //JWT Token Filter VALID OR NOT
//              .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//              .userDetailsService(customUserDetailsService)
//              .build();
//    }
//
//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//}
